<?php
namespace App\Helpers\Abstracts;

abstract class WkpAvailableEnum {
    const Full = 'full';
    const Partial = 'partial';
    const Not = 'not';
}

abstract class WkpImgFormat {
    const Mini = ['NAME' => 'mini', 'W' => 80, 'H' => 80];
    const Small = ['NAME' => 'small', 'W' => 400, 'H' => 400];
    const Large = ['NAME' => 'large', 'W' => 960, 'H' => null];
}