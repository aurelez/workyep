<?php

function _env($key){
    return array_key_exists($key, CONFIG) ? CONFIG[$key] : null;
}

const CONFIG = [
    'FRONTAPP_URL' => 'http://localhost:4200',
    'FRONTAPP_DASHBOARD' => '/user/dashboard',
    'FRONTAPP_EMAILCHECK' => '/action/emailcheck/?token=',

    'REFRESH_TOKEN_NAME' => 'WkpRefreshToken',
    'PASSWORD_CLIENT_ID' => 2,

    'FACEBOOK_ID' => '582686828762762',
    'LINKEDIN_ID' => '776s1mk0bjoeu0',
    'TWITTER_ID' => 'HyEnlFbl8pZ2ihx2kmTltSBDt',
    'GOOGLE_ID' => '784455569932-8td6n1e4lka85j352k4rthhacje9cjqf',
];
