<?php
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Helpers\Abstracts\WkpImgFormat;

function _storeAvatarFromLink($link, $uid) {
    try {
        $contents = file_get_contents($link);
        $name = 'avatar.jpg';
        $dir = "public/users/{$uid}/";
        Storage::makeDirectory($dir);
        Storage::put($dir.$name, $contents);
        _generateImageCrop($dir, $name, WkpImgFormat::Mini);
        _generateImageCrop($dir, $name, WkpImgFormat::Small);
        Storage::delete($dir.$name);
        return $name;
    } catch(Exception $e) {
        Log::alert($e);
        return null;
    }
}

function _generateImageCrop($dir, $name, $format) {
    $dir = 'app/'.$dir;
    if($format['NAME'] === WkpImgFormat::Large['NAME']) {
        Image::make(storage_path($dir.$name))
            ->resize($format['W'], $format['H'], function($constraint){
                $constraint->upsize();
                $constraint->aspectRatio();
            })
            ->encode('jpg', 75)
            ->save(storage_path("{$dir}{$format['NAME']}_{$name}"));

    } else {
        Image::make(storage_path($dir.$name))
            ->fit($format['W'], $format['H'], function($constraint){
                $constraint->upsize();
            })
            ->encode('jpg', 75)
            ->save(storage_path("{$dir}{$format['NAME']}_{$name}"));
    }
}

function _getUserAvatars($uid, $avatar) {
    $mini =  WkpImgFormat::Mini['NAME'];
    $small =  WkpImgFormat::Small['NAME'];
    $uncache = '?w='.uniqid();
    return [
        'mini' => asset("storage/users/{$uid}/{$mini}_{$avatar}{$uncache}"),
        'small' => asset("storage/users/{$uid}/{$small}_{$avatar}{$uncache}")
    ];
}