<?php

namespace App\Http\Controllers\Api\Auth;
use App\User;

trait IssueTokenTrait {

    // Proxy a request to the OAuth server.
    public function _issueToken($grantType, array $params){
        
        $params = array_merge($params, [
            'grant_type'    => $grantType,
            'client_id'     => $this->oAuthClient->id,
            'client_secret' => $this->oAuthClient->secret
        ]);

        //dd($params);
 
        $response = $this->apiConsumer->post('/oauth/token', $params);

        if (!$response->isSuccessful()) {
            return $response;
        }

        $oAuthData = json_decode($response->getContent());

        return response()->json([
            'oauth' => [
                'token_type' => $oAuthData->token_type,
                'access_token' => $oAuthData->access_token,
                'expires_in' => $oAuthData->expires_in
            ]

        // attach the refresh token cookie
        ], 200)->cookie(
            _env('REFRESH_TOKEN_NAME'),
            $oAuthData->refresh_token,
            14400, //min - 10 days
            null,
            null,
            false,
            true // HttpOnly
        );
    }
}
// lien utile: http://esbenp.github.io/2017/03/19/modern-rest-api-laravel-part-4/