<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Application;

use Laravel\Passport\Client;
use App\User;

class LoginController extends Controller{

    use IssueTokenTrait;

    private $cookie;
    private $oAuthClient;
    private $apiConsumer;

    // __construct
    public function __construct(Application $app){
        $this->cookie = $app->make('cookie');
        $this->oAuthClient = Client::find(_env('PASSWORD_CLIENT_ID'));
        $this->apiConsumer = $app->make('apiconsumer');
    }

    // Route:Public /login
    public function login(Request $request){
        //check if user exist
        $credentials = [
            'username' => request('email'),
            'password' => request('password')
            //'scope' => '*'
        ];

        $user = User::where('email', $credentials['username'])->first();

        if(is_null($user)){
            return response()->json([
                'code' => 'WKP0003:',
                'message' => trans('all.loginUserNotFound')
            ], 400);
        }

        return $this->_issueToken('password', $credentials); // from IssueTokenTrait
    }

    // Route:Public /refresh
    public function refresh(Request $request){
        //$refreshToken = $request->header(_env('REFRESH_TOKEN_NAME'));
        $refreshToken = $request->cookie(_env('REFRESH_TOKEN_NAME'));
        return $this->_issueToken('refresh_token', ['refresh_token' => $refreshToken]);
    }


    // Route:Private /logout
    public function logout(Request $request){

        $accessToken = Auth::user()->token();
        // revoke access token
        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update(['revoked' => true]);

        $accessToken->revoke();

        // Also instruct the client to forget the refresh cookie.
        $this->cookie->queue($this->cookie->forget(_env('REFRESH_TOKEN_NAME')));

        //dd($accessToken);
        return response()->json([
            'message' => 'logout'
        ], 204);
    }
}
// lien utile: http://esbenp.github.io/2017/03/19/modern-rest-api-laravel-part-4/