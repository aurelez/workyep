<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Application;

use Laravel\Passport\Client;
use App\User;

class RegisterController extends Controller{

    use IssueTokenTrait;

    private $cookie;
    private $oAuthClient;
    private $apiConsumer;

    // __construct
    public function __construct(Application $app){
        $this->cookie = $app->make('cookie');
        $this->oAuthClient = Client::find(_env('PASSWORD_CLIENT_ID'));
        $this->apiConsumer = $app->make('apiconsumer');
    }

    public function register(Request $request){

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
        
        $user = User::create([
            'name' => request('name'),
            'last_name' => request('last_name'),
            'email' => request('email'),
            'password' => bcrypt( request('password') )
        ]);


        $credentials = [
            'username' => request('email'),
            'password' => request('password'),
            'scope' => '*'
        ];

        return $this->_issueToken('password', $credentials); // from IssueTokenTrait
    }
}
