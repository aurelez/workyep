<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Notifications\ResetPassword;
use Illuminate\Support\Facades\App;
use App\User;

class ResetPasswordController extends Controller{

    use SendsPasswordResetEmails;
    
    public function index(Request $request, $action){

        switch($action){
            case 'email':
                return $this->sendResetEmail($request);
            case 'reset':
                $PR = new ProcessReset();
                return $PR->reset($request);
            default:
                return response()->json([
                    'code' => 'todo:',
                    'message' => 'Bad action value, Unprocessable Entity'
                ], 422);
        }
    }

    private function sendResetEmail(Request $request){
        Validator::make($request->all(), [
            'email' => 'required|email'
        ])->validate();

        $user = User::where('email', $request->email)->first();

        if($user){
            $token = $this->broker()->createToken($user);
            $user->notify(new ResetPassword([
                    'token' => $token
                ])
            );
            return response()->json(['message' => trans('passwords.sent')], 200);
        } else{
            return response()->json([
                'code' => 'WKP0004:',
                'message' => trans('passwords.user')
            ], 400);
        }
    }
    
}

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
/**
 * ProcessReset Class
 */
class ProcessReset extends Controller{

    use ResetsPasswords;

    public function reset(Request $request){

        Validator::make($request->all(), [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ])->validate();

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        if ($request->wantsJson()) {
            if ($response == Password::PASSWORD_RESET) {
                return response()->json(['message' => trans('passwords.reset')], 200);
            } else {
                return response()->json([
                    'code' => 'WKP0005',
                    'message' => trans($response)
                ], 400);
            }
        }
    }
}