<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Models\SocialAccount;

use Socialite;

class SocialAuthController extends RegisterController{

    public function socialAuth(Request $request){

        $providersSansToken = ['linkedin'];
        $credentials = [];

        if(in_array($request->provider, $providersSansToken)){
            Validator::make($request->all(), [
                'provider' => 'required',
                'provider_uid' => 'required',
                'email' => 'required|email',
                'name' => 'required'
            ])->validate();
    
            $credentials = [
                'provider' => $request->provider,
                'provider_uid' => $request->provider_uid,
                'email' => $request->email,
                'name' => $request->name,
                'last_name' => $request->last_name,
                'avatar' => $request->avatar
            ];

        }else{
            Validator::make($request->all(), [
                'provider' => 'required',
                'token' => 'required'
            ])->validate();
    
            $user = Socialite::driver($request->provider)->stateless()->userFromToken($request->token);

            $credentials = [
                'provider' => $request->provider,
                'provider_uid' => $user->getId(),
                'email' => $user->getEmail(),
                'name' => $user->getName(),
                'last_name' => $user->getNickname(),
                'avatar' => $user->getAvatar(),
            ];

            Validator::make($credentials, [
                'provider_uid' => 'required',
                'email' => 'required|email',
                'name' => 'required'
            ])->validate();
        }

        return $this->requestToken($credentials);
    }

    /**
     * requestToken
     *
     * @param array $credentials
     * @return void
     */
    private function requestToken(array $credentials) {

        $socialAccount = SocialAccount::where('provider', $credentials['provider'])
                                        ->where('provider_uid', $credentials['provider_uid'])
                                        ->first();

        if($socialAccount){
            // un socialAccount ne peut exister sans un user
            if($socialAccount->user->email === $credentials['email']){
                return $this->_issueToken('social', $credentials); // from IssueTokenTrait
            }else{
                return response()->json([
                    'code' => 'WKP0006',
                    'message' => 'Email not match'
                ], 400);
            }
        }

        $user = User::where('email', $credentials['email'])->first();

        if($user){
            $this->addSocialAccountToUser($credentials, $user);
        }else{
            try{
                $user = $this->createUserAccount($credentials);
                if(!empty($credentials['avatar'])) {
                    $avatar = _storeAvatarFromLink($credentials['avatar'], $user->id);
                    if(!is_null($avatar)){
                        $user->avatar = $avatar;
                        $user->save();
                    }
                }
            }catch(\Exception $e){
                Log::error("Unable to create user account: {$e}");
                return response("An error occured, please retry later", 422);
            }
        }

        return $this->_issueToken('social', $credentials); // from IssueTokenTrait
    }

    /**
     * addSocialUserArray
     *
     * @param array $credentials
     * @param User $user
     * @return void
     */
    private function addSocialAccountToUser(array $credentials, User $user){
        //dd($user, $credentials);
        Validator::make($credentials, [
            'provider' => ['required', Rule::unique('social_accounts')->where(function($query) use ($user){
                return $query->where('user_id', $user->id);
            })],
            'provider_uid' => 'required'
        ])->validate();

        $user->socialAccount()->create([
            'provider' => $credentials['provider'],
            'provider_uid' => $credentials['provider_uid']
        ]);
    }

    /**
     * createUserAccount
     *
     * @param array $credentials
     * @return void
     */
    private function createUserAccount(array $credentials){
        $user = null;
        DB::transaction( function() use ($credentials, &$user){
    		$user = User::create([
    			'email' => $credentials['email'],
    			'name' => $credentials['name'],
    			'last_name' => $credentials['last_name']
    		]);
    		$this->addSocialAccountToUser($credentials, $user);
        });

        return $user;
    }
}