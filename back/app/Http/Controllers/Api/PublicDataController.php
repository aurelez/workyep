<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\DefaultMail;
use App\Models\Profile;

class PublicDataController extends Controller{
    public function publicData(Request $request){
        $data = [
            'configs' => [
                'socials' => [
                    'twitter' => _env('TWITTER_ID'),
                    'facebook' => _env('FACEBOOK_ID'),
                    'linkedin' => _env('LINKEDIN_ID'),
                    'google' => _env('GOOGLE_ID')
                ]
            ]
        ];
        return response()->json(['data'=> $data]);
    }

    public function getProfile(Request $request, $uid){
        $user = User::find($uid);
        if(!$user){
            return 404;
        }
        return response()->json([
            'profile' =>  $user
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    
    // -- router postEmailcheck
    public function postEmailcheck(Request $request){

        Validator::make($request->all(), ['token' => 'required'])->validate();
        $profile = Profile::where('emailcheck_token', $request->token)->first();

        if(!is_null($profile)) {
            if($profile->email_check) {
                return response()->json(['code' => 'WKP1001:', 'message' => trans('all.emailAlreadyCheck')], 400);
            }
            
            $profile->email_check = true;
            $profile->emailcheck_token = null;
            $profile->save();
            
            Mail::to($profile->user)->send(new DefaultMail([
                'subject' => trans('all.emailCheckTitle'),
                'name' => $profile->user->name,
                'body' => trans('all.emailCheckValidate'),
                'link' => _env('FRONTAPP_URL')._env('FRONTAPP_DASHBOARD')
            ]));
            return response()->json([
                'code' => 'WKP1002:',
                'message' =>  trans('all.emailCheckValidate')
            ], 200, [], JSON_NUMERIC_CHECK);
        } else {
            return response()->json(['code' => 'WKP1003:', 'message' => trans('all.emailCheckInvalidToken')], 400);
        }
        
    }
}
