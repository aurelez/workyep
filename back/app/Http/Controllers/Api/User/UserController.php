<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailCheckToken;
use App\Mail\DefaultMail;
use App\User;
use App\Helpers\Abstracts\WkpAvailableEnum;
use App\Models\Skill;
use App\Models\UserSkill;

class UserController extends Controller {

    // -- constr
    public function __construct() {
    }

    // -- router getUserBase
    public function getUserBase(Request $request){
        $user = Auth::user();
        return $this->retUserbase($user);
    }

    // -- router updateProfile
    public function updateProfile(Request $request) {
        $user = Auth::user();

        Validator::make($request->all(), [
            'title' => 'required|max:60',
            'about' => 'required|max:500',
            'available' => 'required|boolean',
            'hourly_rate' => 'required|numeric',
            'experience_year' => 'required|numeric',
            'response_time' => 'required|numeric',
            'country' => 'required',
            'city' => 'required',
            'work_out' => 'required'
        ])->validate();

        $user->profile()->updateOrCreate(['user_id' => $user->id], $request->all());

        return response()->json([
            'user' =>  $user,
            'profile' => $user->profile
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    // -- router available
    public function available(Request $request) {
        $user = Auth::user();
        Validator::make($request->all(), ['available' => 'required'])->validate();

        $available =  WkpAvailableEnum::Not;
        if ($request->available != WkpAvailableEnum::Not) {
            $available =  WkpAvailableEnum::Full;
        }
        $user->profile()->update(['available' => $available]);
        return response()->json([
            'available' =>  $user->profile->available
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    // -- router profilePhoto
    public function profilePhoto(Request $request) {
        $user = Auth::user();
        Validator::make($request->all(), ['avatar' => 'required'])->validate();

        $fileLink = $request->file('avatar');
        $avatar = _storeAvatarFromLink($fileLink, $user->id);
        if(!is_null($avatar)){
            $user->avatar = $avatar;
            $user->save();
        }
        return response()->json([
            'avatar' =>  _getUserAvatars($user->id, $user->avatar)
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    // -- router getEmailcheck
    public function getEmailcheck(Request $request) {
        $user = Auth::user();
        if($user->profile->email_check){
            return response()->json(['code' => 'WKP1001:', 'message' => trans('all.emailAlreadyCheck')], 400);
        }
        // send the token
        $token = $user->id.bin2hex(random_bytes(32));
        $user->profile->emailcheck_token = $token;
        $user->profile->save();

        Mail::to($user)->send(new EmailCheckToken([
            'name' => $user->name,
            'link' => _env('FRONTAPP_URL')._env('FRONTAPP_EMAILCHECK').$token
        ]));

        return response()->json([
            'code' => 'WKP1004:',
            'message' =>  trans('all.emailCheckSent')
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    // -- router findSkills
    public function findSkills(Request $request) {
        $skills = Skill::where(function($query) use ($request) {
            $query->where('name', 'LIKE', "$request->search%");
        })->take(6)->get();

        $skillsData = [];
        foreach($skills as $skill){
            array_push($skillsData, [
                'name' => $skill->name,
                'value' => $skill->id,
                'iconClass' => $skill->icon
            ]);
        }

        return response()->json([
            'success' => true,
            'results' =>  $skillsData
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    // -- router skill
    public function skill(Request $request) {
        Validator::make($request->all(), ['id' => 'required'])->validate();
        $pid = Auth::user()->profile->id;

        if ($request->isMethod('delete')) {
            UserSkill::where('profile_id', $pid)
                ->where('skill_id', $request->id)
                ->delete();
            return response()->json([], 202);
        } else {
            // limite numer of skills by profile
            UserSkill::firstOrCreate([
                'profile_id' => $pid,
                'skill_id' => $request->id
            ]);
            return response()->json([], 201);
        }
    }

    // -- parts
    public function retUserbase(User $user) {
        if($user->avatar) {
            $user->avatar = _getUserAvatars($user->id, $user->avatar);
        }

        if($user->profile) {
            $user->profile->skills = DB::table('skills')->select('skills.id as value', 'skills.name')
                                    ->join('user_skills', 'user_skills.skill_id', 'skills.id')
                                    ->join('profiles', 'profiles.id', 'user_skills.profile_id')
                                    ->where('profiles.id', $user->profile->id)->get();
        }

        return response()->json([
            'data' =>  [
                'user' =>  $user,
                'profile' => $user->profile
            ]
        ], 200, [], JSON_NUMERIC_CHECK);
    }
}
