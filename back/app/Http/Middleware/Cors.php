<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\App;

class Cors{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        // set Locale language
        if( $request->header('Accept-Language') ){
            $lang = $request->header('Accept-Language');
            if( !empty($lang) && $lang != 'en' )
                App::setLocale($lang);
        }

        $trustedOrigins = [
            'http://localhost:4200'
        ];
        
        // note: custom request header (WkpRefresh) can be used for specific data
        $headers = [
            'Access-Control-Allow-Origin' => $trustedOrigins,
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, HEAD',
            'Access-Control-Allow-Headers' => 'Content-Type, Accept, Origin, Authorization'
        ];

        if($request->method() == "OPTIONS") {
            return Response::make('OK', 200, $headers);
        }

        $response = $next($request);
        foreach($headers as $key => $value)
            $response->header($key, $value);

        return $response;
    }
}
