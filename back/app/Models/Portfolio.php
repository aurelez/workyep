<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    //-- Profile relation
    public function user(){
        return $this->belongsTo(Profile::class);
    }

    //-- UserSkill relation 
    public function portfolioItem(){
        return $this->hasMany(PortfolioItem::class);
    }
}
