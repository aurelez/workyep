<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioItem extends Model
{
    //-- Portfolio relation
    public function portfolio(){
        return $this->belongsTo(Portfolio::class);
    }
}
