<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Portfolio;
use App\Models\UserSkill;

class Profile extends Model{

    protected $guarded = [];

    //-- User relation
    public function user(){
        return $this->belongsTo(User::class);
    }
    
    //-- Portfolio relation 
    public function portfolio(){
        return $this->hasMany(Portfolio::class);
    }

    //-- UserSkill relation 
    public function userSkill(){
        return $this->hasMany(UserSkill::class);
    }
}
