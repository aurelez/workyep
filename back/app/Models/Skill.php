<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model{

    //-- Profile relation
    public function user(){
        return $this->belongsTo(Profile::class);
    }
}
