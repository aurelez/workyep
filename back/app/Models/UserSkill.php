<?php

namespace App\Models;
use App\User;

use Illuminate\Database\Eloquent\Model;

class UserSkill extends Model{

    protected $fillable = ['profile_id', 'skill_id'];

    //-- Profile relation
    public function profile(){
        return $this->belongsTo(Profile::class);
    }
}
