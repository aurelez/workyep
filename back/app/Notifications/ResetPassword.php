<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    use Queueable;

    private $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $data){
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable){
        //dd($this->data);
        $url = _env('FRONTAPP_URL').'/action/password?token='.$this->data['token'];
        return (new MailMessage)
                    ->subject(trans('passwords.subject'))
                    ->greeting(trans('all.hi').$notifiable->name.',')
                    ->line(trans('passwords.content'))
                    ->action(trans('passwords.subject'), url($url))
                    ->line(trans('passwords.contentIgnore'))
                    ->line(trans('all.wkpThanks'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
