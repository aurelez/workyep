<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\Abstracts\WkpAvailableEnum;

class CreateProfilesTable extends Migration
{
    const AVAILABLE_ENUM = [
        WkpAvailableEnum::Full,
        WkpAvailableEnum::Partial,
        WkpAvailableEnum::Not
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('title')->nullable();
            $table->text('about')->nullable();
            $table->boolean('email_check')->default(false);
            $table->enum('available', self::AVAILABLE_ENUM)->default(WkpAvailableEnum::Full);
            $table->enum('devise', ['CAD', 'USD', 'XOF'])->default('CAD');
            $table->boolean('work_out')->default(false);
            $table->unsignedSmallInteger('hourly_rate')->nullable();
            $table->unsignedTinyInteger('experience_year')->nullable();
            $table->unsignedTinyInteger('response_time')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('emailcheck_token')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
