<?php

return [
    'hi' => "Hi ",
    'wkpThanks' => "Thank you for using our application! ",
    'dashboardConnect' => "Connect to your dashboard ",
    'emailCheckTitle' => "Email Validation",
    'emailCheckToken' => "Click the the link below to validate your email.",
    'emailCheckBtn' => "Validate your email",
    'emailCheckSent' => 'We have e-mailed your validation link!',
    'emailCheckInvalidToken' => 'The email validation token is invalid, use the link you received by email.',
    'emailCheckValidate' => 'Your email has been validated!',
    'emailAlreadyCheck' => 'Your email is already validated',

    'loginUserNotFound' => 'No users found, check the require fields and try again.',

];