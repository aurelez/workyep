<?php

return [
    'hi' => "Hi ",
    'wkpThanks' => "Thank you for using our application! ",
    'dashboardConnect' => "Connect to your dashboard ",
    'emailCheckTitle' => "Email Validation",
    'emailCheckToken' => "Click the the link below to validate your email.",
    'emailCheckBtn' => "Validate your email",
    'emailCheckSent' => 'We have e-mailed your validation link!',
    'emailCheckInvalidToken' => 'The email validation token is invalid, utilisez le lien que vous avez reçu par email',
    'emailCheckValidate' => 'Your email has been validated!',
    'emailAlreadyCheck' => 'Your email is already validated',

    'loginUserNotFound' => 'Aucun utilisateur trouvé, vérifiez les champs requis et réessayez.',

];