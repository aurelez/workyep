@component('mail::message')
# {{ trans('all.hi'). $data['name'] }},

{{ $data['body'] }}

@component('mail::button', ['url' => $data['link']])
{{ trans('all.dashboardConnect') }}
@endcomponent
{{$data['link']}}

{{ trans('all.wkpThanks') }},<br>
{{ config('app.name') }}
@endcomponent
