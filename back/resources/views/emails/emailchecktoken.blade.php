@component('mail::message')
# {{ trans('all.hi'). $data['name'] }},
### {{ trans('all.emailCheckTitle') }}
{{ trans('all.emailCheckToken') }}

@component('mail::button', ['url' => $data['link']])
{{ trans('all.emailCheckBtn') }}
@endcomponent
{{$data['link']}}

{{ trans('all.wkpThanks') }},<br>
{{ config('app.name') }}
@endcomponent
