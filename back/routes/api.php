<?php

use Illuminate\Http\Request;

Route::namespace('Api')->group(function () {
    /*------------------------------------
    Public Routes 
    --------------------------------------*/
    /* *** Auth *** */
    Route::prefix('auth')->namespace('Auth')->group(function () {
        Route::post('register', 'RegisterController@register')->name('w.register');
        Route::post('login', 'LoginController@login')->name('w.login');
        Route::post('refresh', 'LoginController@refresh')->name('w.refresh');
        Route::post('social', 'SocialAuthController@socialAuth')->name('w.socialAuth');
        Route::post('password/{action}', 'ResetPasswordController@index')->name('w.password');
    });

    /* *** Forall Access *** */
    Route::prefix('forall')->group(function () {
        Route::get('publicdata', 'PublicDataController@publicdata')->name('w.forall.publicdata');
        Route::get('profile/{uid}', 'PublicDataController@getProfile')->name('w.forall.profile');
        Route::post('emailcheck', 'PublicDataController@postEmailcheck')->name('w.forall.emailcheck');
    });

    /*------------------------------------
    Private Routes 
    --------------------------------------*/
    Route::middleware(['auth:api'])->group(function () {

        /* *** Auth *** */
        Route::prefix('auth')->namespace('Auth')->group(function () {
            Route::post('logout', 'LoginController@logout')->name('w.logout');
        });

        /* *** User *** */
        Route::prefix('user')->namespace('User')->group(function () {
            Route::get('base', 'UserController@getUserBase')->name('w.user.base');
            Route::get('emailcheck', 'UserController@getEmailcheck')->name('w.user.emailcheck');
            Route::get('findskills/{search}', 'UserController@findSkills')->name('w.user.findskills');

            Route::post('profile', 'UserController@updateProfile')->name('w.user.profile');
            Route::post('available', 'UserController@available')->name('w.user.available');
            Route::post('profilephoto', 'UserController@profilePhoto')->name('w.user.profilephoto');
            
            Route::match(['post', 'delete'], 'skill', 'UserController@skill')->name('w.user.skill');
        });

    });
    
});