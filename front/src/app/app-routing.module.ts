import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './m_shared/guard/auth.guard';
import { environment as ENV } from '../environments/environment';

import { SiteLayoutComponent } from './components/_layout/site-layout/site-layout.component';
import { AppLayoutComponent } from './components/_layout/app-layout/app-layout.component';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/user/dashboard/dashboard.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ForactionComponent } from './components/foraction/foraction.component';

const authRoutes = [
  { path: 'dashboard' }
];

const routes: Routes = [
  // Site routes
  { path: '',
    component: SiteLayoutComponent,
    children: [
      { path: '', component: HomeComponent, pathMatch: 'full'},
      { path: ENV.routes.login, component: LoginComponent },
      { path: ENV.routes.action, component: ForactionComponent },
    ]
  },

  // Account routes
  { path: '',
    component: AppLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: ENV.routes.userDashboard, component: DashboardComponent },
    ]
  },

  // -- 404
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
