import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { PublicService, UserService } from './services/';
import { SharedService, HelperService } from './m_shared/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  constructor(private _publicService: PublicService,
              private _helperSevice: HelperService,
              private _userService: UserService,
              private _translateService: TranslateService) {

    this._translateService.setDefaultLang('en');
    this._translateService.use('en');
  }

  ngOnInit() {
    this.getPublicBase();
    this.getUserBase();
  }

  getPublicBase() {
    this._publicService.getAll().subscribe();
  }

  getUserBase() {
    this._helperSevice.isAuth$.subscribe(
      res => {
        if (res) {
          this._userService.getUserBase().subscribe();
        }
      }
    );
  }
}
