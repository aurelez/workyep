import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { SharedModule } from './m_shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { AuthModule } from './m_auth/auth.module';

import { HttpInterceptorService } from './m_shared/services';

import { AppComponent } from './app.component';
// any
import { SiteLayoutComponent } from './components/_layout/site-layout/site-layout.component';
import { AppLayoutComponent } from './components/_layout/app-layout/app-layout.component';
import { HeaderComponent } from './components/_layout/header/header.component';
import { FooterComponent } from './components/_layout/footer/footer.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
// all
import { HomeComponent } from './components/home/home.component';
// auth
import { LoginComponent } from './components/login/login.component';
// user
import { DashboardComponent } from './components/user/dashboard/dashboard.component';
import { ForactionComponent } from './components/foraction/foraction.component';
import { DevisePipe } from './pipe/devise.pipe';

@NgModule({
  imports: [
    BrowserModule.withServerTransition({appId: 'workyep'}),
    AppRoutingModule,
    HttpClientModule,
    TranslateModule,
    SharedModule,
    AuthModule
  ],
  declarations: [
    AppComponent,
    SiteLayoutComponent,
    AppLayoutComponent,
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    HomeComponent,
    LoginComponent,
    DashboardComponent,
    ForactionComponent,
    DevisePipe,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
