import { Component, OnInit } from '@angular/core';
import { SharedService, MarkupService } from '../../../m_shared/services';
import { User } from '../../../models';
import { AuthService } from '../../../m_auth/services';
import { UserService } from '../../../services';
import { TranslateService } from '@ngx-translate/core';

declare const $: any, toastr: any;

const AVAILABLE = { NOT: 'not', FULL: 'full' };

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss']
})
export class AppLayoutComponent implements OnInit {

  public userBase: User;
  public isLoading = true;
  public phCircleImg: string;
  public phSingle: string;

  constructor(private _sharedService: SharedService,
              private _markupService: MarkupService,
              private _authService: AuthService,
              private _userService: UserService,
              private _translateService: TranslateService) {

  }

  ngOnInit() {
    this.phCircleImg = this._markupService.phCircleImage('dark');
    this.phSingle = this._markupService.phSingle('dark');
    this._sharedService.userBase$.subscribe(res => {
      if (res) {
        this.isLoading = false;
        this.userBase = res;
        this.initDomAfterUserBaseReady();
      }
    });
  }

  setAvailability(e) {
    let payload = {available: AVAILABLE.NOT};
    if (e.target.checked) {
      payload = {available: AVAILABLE.FULL};
    }
    this._userService.available(payload).subscribe(
      res => {
        this._translateService.get('all.saved').subscribe(r => {
          toastr.success(r);
        });
      }
    );
  }

  initDomAfterUserBaseReady() {
    if ($('.ui.dropdown').length > 0) {
      $('.ui.dropdown').dropdown({
        on: 'click',
        direction: 'downward'
      });

      $('.ui.checkbox').checkbox();

    } else { window.setTimeout(() => this.initDomAfterUserBaseReady(), 100); }
  }

  logout() {
    this._authService.logout();
  }
}
