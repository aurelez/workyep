import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { HelperService, SharedService, MarkupService } from '../../../m_shared/services';
import { AuthService } from '../../../m_auth/services';
import { User } from '../../../models';

declare const $: any;

const LOGIN_URL = '/login';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public isAuth = false;
  public isLoginPage = false;
  public userBase: User;
  public phSingle: any;

  constructor(private _authService: AuthService,
              private _routerService: Router,
              private _markupService: MarkupService,
              private _helperService: HelperService,
              private _sharedService: SharedService) {
  }

  ngOnInit() {
    this._helperService.isAuth$.subscribe(res => { this.isAuth = res; });
    this.setIsLoginPage();
    this.phSingle = this._markupService.phSingle();
    this._sharedService.userBase$.subscribe(res => {
      if (res) {
        this.phSingle = false;
        this.userBase = res;
        this.initDropdown();
      }
    });
  }

  initDropdown() {
    if ($('.ui.dropdown.detail').length > 0) {
      $('.ui.dropdown.detail').dropdown({
        on: 'click',
        direction: 'downward'
      });
    } else { window.setTimeout(() => this.initDropdown(), 100); }
  }

  setIsLoginPage() {
    this.isLoginPage = (window.location.pathname === LOGIN_URL);
    this._routerService.events.subscribe(
      res => {
        if (res instanceof NavigationEnd) {
          this.isLoginPage = (res.urlAfterRedirects === LOGIN_URL);
        }
      }
    );
  }

  toogleLoginModal(isToRegister: boolean) {
    this._sharedService.setIsClickToRegister(isToRegister);
    $('.loginModal').modal('show');
  }

  logout() {
    this._authService.logout();
  }
}
