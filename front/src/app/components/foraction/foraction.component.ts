import { Component, OnInit } from '@angular/core';
import { environment as ENV } from '../../../environments/environment';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AbstractComponentHelper } from '../../m_shared/abstract-component-helper';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService, MarkupService } from '../../m_shared/services';
import { PublicService } from '../../services';


@Component({
  selector: 'app-foraction',
  templateUrl: './foraction.component.html',
  styleUrls: ['./foraction.component.scss']
})
export class ForactionComponent extends AbstractComponentHelper implements OnInit {

  public action: string;
  public questing = false;
  public rResetForm: FormGroup;
  public isLost = false;
  public pwdChangeDone = false;

  constructor(protected _translateService: TranslateService,
              protected _markupService: MarkupService,
              protected _helperService: HelperService,
              private _formBuilderService: FormBuilder,
              private _publicService: PublicService,
              private _activatedRouteService: ActivatedRoute,
              private _routerService: Router) {

    super(_helperService, _translateService, _markupService);
  }

  ngOnInit() {
    this.init();
  }

  private init() {
    this._activatedRouteService.params.subscribe(params => {
      this.action = (params.action);

      switch (this.action) {
        case 'emailcheck':
          this.initEmailCheck();
          break;
        case 'password':
          this.initPasswordReset();
          break;
        default:
          this.isLost = true;
          window.setTimeout(() => {
            this._routerService.navigate([ENV.routes.login]);
          }, 4000);
      }
    });
  }

  // -- emailcheck
  private initEmailCheck() {
    this.messageBloc_ = false;
    this.questing = true;
    this._activatedRouteService.queryParams.subscribe(params => {

      this._publicService.emailCheck({token: params.token}).subscribe(
        res => {
          this.questing = false;
          this.messageBloc_ = this._markupService.sucess(res.message);
          window.setTimeout(() => {
            this.initResetForm();
            this._routerService.navigate([ENV.routes.login]);
          }, 6000);
        },
        error => {
          this.questing = false;
          this.handleHttpError_(error);
        }
      );

    });
  }

  // -- password
  private initPasswordReset() {
    this.messageBloc_ = false;
    this.initResetForm();
  }

  private initResetForm() {
    this.rResetForm = this._formBuilderService.group({
      'email': [null, [Validators.required, Validators.email]],
      'password': [null, [Validators.required]],
      'password_confirmation': [null, [Validators.required]],
      'token': [null],
    });
  }

  resetPwdSubmit() {
    this.questing = true;
    this._activatedRouteService.queryParams.subscribe(params => {

      this.rResetForm.get('token').setValue(params.token);
      this._publicService.resetPassword(this.rResetForm.value).subscribe(
        res => {
          this.questing = false;
          this.messageBloc_ = this._markupService.sucess(res.message);
          this.pwdChangeDone = true;
          window.setTimeout(() => {
            this.initResetForm();
            this._routerService.navigate([ENV.routes.login]);
          }, 6000);
        },
        error => {
          this.questing = false;
          this.handleHttpError_(error);
        }
      );

    });
  }

  isInvalidField(field) {
    return !this.rResetForm.controls[field].valid && this.rResetForm.controls[field].touched;
  }

}
