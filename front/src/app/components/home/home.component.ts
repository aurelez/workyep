import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelperService, SharedService } from '../../m_shared/services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private _routerService: Router,
              private _sharedService: SharedService,
              private _helperService: HelperService) {
  }

  ngOnInit() {
  }

  test() {
    this._sharedService.setInvitLoginMsg({msg: 'login.signupBefore', target: 'Jesus'});
  }
}
