import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelperService } from '../../m_shared/services';
import { environment as ENV } from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private _routerService: Router,
              private _helperService: HelperService) {
  }

  ngOnInit() {
    if (this._helperService.isAuth()) {
      this._routerService.navigate([ENV.routes.userDashboard]);
    }
  }

}
