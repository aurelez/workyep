import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment as ENV } from '../../../../environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AbstractComponentHelper } from '../../../m_shared/abstract-component-helper';
import { UserService } from '../../../services/';
import { SharedService, HelperService, MarkupService } from '../../../m_shared/services';

import { Chart } from 'chart.js';
import { User } from '../../../models';
import { TranslateService } from '@ngx-translate/core';

declare const $: any, toastr: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends AbstractComponentHelper implements OnInit, OnDestroy {

  public userBase: User;
  public isLoading = true;
  public isRequesting = false;
  public emailCheckQuesting = false;

  public isPhotoFileTooBig = false;
  public profilePictureName: string;
  public selectedProfilePhoto: File = null;
  public rphotoChangeForm: FormGroup;

  constructor(protected _translateService: TranslateService,
              protected _markupService: MarkupService,
              protected _helperService: HelperService,
              private _formBuilderService: FormBuilder,
              private _userService: UserService,
              private _sharedService: SharedService) {

    super(_helperService, _translateService, _markupService);

  }

  ngOnInit() {
    this._sharedService.userBase$.subscribe(res => {
      this.userBase = res;
      this.isLoading = false;
      this.domAfterUserBaseReady();
    });
    this.initChangePhotoForm();
  }

  ngOnDestroy() {
    // remove de modal from the DOM
    $('.ui.dimmer .photoChangeModal').remove();
  }

  domAfterUserBaseReady() {
    if ($('.userbaseInner').length > 0) {
      this.feedDataToChart();
      this.initSkillsDropdown(this);
    } else { window.setTimeout(() => this.domAfterUserBaseReady(), 100); }
  }

  getSmallDesc() {
    return this.userBase.profile.about.substr(0, 80) + '...' || null;
  }

  initChangePhotoForm() {
    this.rphotoChangeForm = this._formBuilderService.group({
      'avatar': [null, Validators.required]
    });
  }

  togglePhotoChangeModal() {
    $('.photoChangeModal')
      .modal({
        onApprove: () => {
          return false;
        }
      }).modal('toggle');
  }

  choosingProfilePictureFile(e) {
    this.messageBloc_ = false;
    this.profilePictureName = '';
    const file = e.target.files[0];
    if (file) {
      this.profilePictureName = file.name.substr(0, 24);
      if (file.size > ENV.configs.maxFileSize) {
        this.setErrorMessage_('error.photoSize');
        this.isPhotoFileTooBig = true;
      } else {
        this.selectedProfilePhoto = <File>file;
        this.isPhotoFileTooBig = false;
      }
    }
  }

  ProfilePictureSubmit() {
    this.messageBloc_ = false;
    if (!this.isRequesting) {
      this.isRequesting = true;

      if (this.rphotoChangeForm.invalid) {
        this.setErrorMessage_('error.inalidform');
        this.isRequesting = false;
      } else {
        if (this.isPhotoFileTooBig) {
          this.setErrorMessage_('error.photoSize');
          this.isRequesting = false;
        } else {
          const payload = new FormData();
          payload.append('avatar', this.selectedProfilePhoto, this.selectedProfilePhoto.name);

          this._userService.changeProfilePhoto(payload).subscribe(
            res => {
              this.isRequesting = false;
              this.profilePictureName = null;
              this.rphotoChangeForm.get('avatar').setValue(null);
              this.togglePhotoChangeModal();
            },
            error => {
              this.isRequesting = false;
              this.profilePictureName = null;
              this.rphotoChangeForm.get('avatar').setValue(null);
              this.handleHttpError_(error);
            }
          );
        }
      }
    }
  }

  sendCheckEmailLink() {
    this.emailCheckQuesting = true;
    this._userService.getEmailCheck().subscribe(
      res => {
        this.emailCheckQuesting = false;
        this.messageBloc_ = this._markupService.sucess(res.message);
      },
      error => {
        this.emailCheckQuesting = false;
        this.handleHttpError_(error);
      }
    );
  }

  feedDataToChart() {
    const labels = {
      sel: '#useApChart',
      label: 'Apparition of your profile in search',
      tooltip: 'Apparition in search: '
    };
    const data = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      data: [0, 10, 5, 2, 20, 30, 45]
    };
    this.initChart(labels, data);
  }

  initChart(labels: {sel: string, label: string, tooltip: string},
    data: {labels: string[], data: number[]}) {

    const ctx = $(labels.sel)[0].getContext('2d');
    const chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',
    // The data for our dataset
    data: {
    labels: data.labels,
    datasets: [{
      label: labels.label,
      backgroundColor: `rgba(${ENV.configs.primaryColorRgb}, .8)`,
      borderColor:  `rgb(${ENV.configs.primaryColorRgb})`,
      borderWidth: 2,
      data: data.data,
    }]
    },
    // Configuration options go here
    options: {
    animation: {
      easing: 'easeInOutCubic'
    },
    tooltips: {
      callbacks: {
        label: function(tooltipItem) {
          return labels.tooltip + Math.round(tooltipItem.yLabel * 100) / 100;
        }
      }
    },
    maintainAspectRatio: false
    }
    });
  }

  initSkillsDropdown(that) {
    const selectedSkills = [];
    this.userBase.profile.skills.forEach(skill => {
      selectedSkills.push(skill.value.toString());
    });

    $('#skillsDropdown').dropdown({
      apiSettings: {
        url: `${ENV.apiBase + ENV.apiUrl.findSkills}/{query}`,
        beforeSend: function(settings) {
          if (settings.urlData.query.length < 1) {
            return false;
          }
          return settings;
        },
        beforeXHR: function(xhr) {
          xhr.setRequestHeader ('Authorization', `Bearer ${that._helperService.getCookie(ENV.configs.cookieAccessToken)}`);
          return xhr;
        },
        onFailure: (response) => {
          // console.log(response);
        },
        cache: false, // use true in prod: prevent resubmit empty response query
        silent: false // true in prod: hide error showing in console
      },
      onAdd: (addedValue, addedText) => {
        if (selectedSkills.includes(addedValue)) {
          return;
        }
        this._userService.addSkill({ id: addedValue }).subscribe(
          res => {
            this._translateService.get('all.saved').subscribe(r => {
              // toastr.options.closeButton = true;
              toastr.success(`<b>${addedText}</b> ${r}`);
            });
          }
        );
      },
      onRemove: (removedValue, removedText) => {
        console.log(removedValue);
        this._userService.removeSkill(removedValue).subscribe(
          res => {
            this._translateService.get('all.removed').subscribe(r => {
              toastr.success(`<b>${removedText}</b> ${r}`);
            });
          }
        );
      }
    })
    .dropdown('setup menu', {
      values: this.userBase.profile.skills
    })
    .dropdown('set selected', selectedSkills);

    $('.skillsBlock input.search')
      .css('min-width', '180px')
      .attr('placeholder', $('.skillsBlock .hiddenInput').attr('placeholder'));
  }

}
