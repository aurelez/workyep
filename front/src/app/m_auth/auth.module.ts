import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { LoginBlockComponent } from './login-block/login-block.component';
import { ResetpwdComponent } from './resetpwd/resetpwd.component';

const COMPONENTS = [];

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoginBlockComponent,
    ResetpwdComponent
  ],
  providers: [],
  exports: [
    LoginBlockComponent,
    ResetpwdComponent
  ]
})
export class AuthModule { }
