import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbstractComponentHelper } from '../../m_shared/abstract-component-helper';

import { AuthService, GoogleService, FacebookService, LinkedinService } from '../services';
import { SharedService, MarkupService, HelperService } from '../../m_shared/services';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

declare const $: any;

const LOGIN_URL = '/login';

@Component({
  selector: 'app-login-block',
  templateUrl: './login-block.component.html',
  styleUrls: ['./login-block.component.scss']
})
export class LoginBlockComponent extends AbstractComponentHelper implements OnInit, OnDestroy {

  public facebookQuesting = false;
  public linkedinQuesting = false;
  public googleQuesting = false;
  public wkpQuesting = false;
  public isRequesting = false;
  public isRegister = false;
  public loginInvitMsg: string;

  public rLoginForm: FormGroup;

  constructor(protected _translateService: TranslateService,
              protected _markupService: MarkupService,
              protected _helperService: HelperService,
              private _authService: AuthService,
              private _sharedService: SharedService,
              private _googleService: GoogleService,
              private _facebookService: FacebookService,
              private _linkedinService: LinkedinService,
              private _ngZoneService: NgZone,
              private _formBuilderService: FormBuilder,
              private _routerService: Router) {

    super(_helperService, _translateService, _markupService);
  }

  ngOnInit() {
    this.initLoginForm();
    this._sharedService.invitLoginMsg$.subscribe(message => this.setLoginInvitMsg(message));
    this._sharedService.isClickToRegister$.subscribe(res => res ? this.setRegister() : this.setLogin() );
  }

  ngOnDestroy() {
    // remove de modal from the DOM
    $('.ui.dimmer .loginModal').remove();
  }

  setLoginInvitMsg(invitMsg) {
    if (!invitMsg) { invitMsg = {msg: 'login.signupInvit', target: false}; }
    if (invitMsg.target) {
      this._translateService.get(invitMsg.msg, {v: invitMsg.target}).subscribe(res => { this.loginInvitMsg = res; });
    } else {
      this._translateService.get(invitMsg.msg).subscribe(res => { this.loginInvitMsg = res; });
    }
  }

  // ***
  googleLogin() {
    if (!this.isRequesting) {
      this.messageBloc_ = false;
      this.isRequesting = this.googleQuesting = true;
      this._googleService.login$().subscribe(
        result => {
          this._ngZoneService.run(() => {
            this.isRequesting = this.googleQuesting = false;
            this.handleAuthCallback(result);
          });
        }
      );
    }
  }

  facebookLogin() {
    if (!this.isRequesting) {
      this.messageBloc_ = false;
      this.isRequesting = this.facebookQuesting = true;
      this._facebookService.login$().subscribe(
        result => {
          this._ngZoneService.run(() => {
            this.isRequesting = this.facebookQuesting = false;
            this.handleAuthCallback(result);
          });
        }
      );
    }
  }

  linkedinkLogin() {
    if (!this.isRequesting) {
      this.messageBloc_ = false;
      this.isRequesting = this.linkedinQuesting = true;
      this._linkedinService.login$().subscribe(
        result => {
          this._ngZoneService.run(() => {
            this.isRequesting = this.linkedinQuesting = false;
            this.handleAuthCallback(result);
          });
        }
      );
    }
  }

  wkpLoginSubmit() {
    if (!this.isRequesting) {
      this.messageBloc_ = false;
      if (this.rLoginForm.invalid) {
        this.setErrorMessage_('error.inalidform');
      } else {
        this.isRequesting = this.wkpQuesting = true;
        if (this.isRegister) {
          this._authService.wkpRegister(this.rLoginForm.value).subscribe(
            result => {
              this.isRequesting = this.wkpQuesting = false;
              this.handleAuthCallback({success: true, message: result});
            },
            error => {
              this.isRequesting = this.wkpQuesting = false;
              this.handleAuthCallback({success: false, message: error});
            },
          );
        } else {
          this._authService.wkpAuth(this.rLoginForm.value).subscribe(
            result => {
              this.isRequesting = this.wkpQuesting = false;
              this.handleAuthCallback({success: true, message: result});
            },
            error => {
              this.isRequesting = this.wkpQuesting = false;
              this.handleAuthCallback({success: false, message: error});
            },
          );
        }
      }
    }
  }

  // ***
  initLoginForm() {
    this.messageBloc_ = false;
    this.rLoginForm = null;
    if (this.isRegister) {
      this.rLoginForm = this._formBuilderService.group({
        'name': [null, Validators.required],
        'last_name': [null],
        'email': [null, [Validators.required, Validators.email]],
        'password': [null, Validators.required]
      });
    } else {
      this.rLoginForm = this._formBuilderService.group({
        'email': [null, [Validators.required, Validators.email]],
        'password': [null, Validators.required]
      });
    }
  }

  // ***
  handleAuthCallback(result) {
    this.messageBloc_ = false;
    if (result.success) {
      this.rLoginForm.reset();
      $('.loginModal').modal('hide');
      this._sharedService.setInvitLoginMsg(null);
      this._routerService.navigate(['user/dashboard']);
    } else { this.handleAuthError(result.message); }
  }

  handleAuthError(data) {
    // console.log(data);
    if (data instanceof HttpErrorResponse) {
      this.handleHttpError_(data);
    } else { this.setErrorMessage_('error.provider'); }
  }

  // ***
  setRegister(e?) {
    if (e) { e.preventDefault(); }
    this.isRegister = true;
    this.initLoginForm();
  }

  setLogin(e?) {
    if (e) { e.preventDefault(); }
    this.isRegister = false;
    this.initLoginForm();
  }

  // ***
  isInvalidField(field) {
    return !this.rLoginForm.controls[field].valid && this.rLoginForm.controls[field].touched;
  }

  toogleForgotPwdModal() {
    this.messageBloc_ = false;
    $('.loginModal').modal('hide');
    window.setTimeout(() => { $('.forgotPwdModal').modal('show');
      }, window.location.pathname === LOGIN_URL ? 2 : 510 );
  }

}
