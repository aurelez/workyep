import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AbstractComponentHelper } from '../../m_shared/abstract-component-helper';
import { AuthService } from '../services';
import { MarkupService, HelperService } from '../../m_shared/services';

declare const $: any;

@Component({
  selector: 'app-resetpwd',
  templateUrl: './resetpwd.component.html'
})
export class ResetpwdComponent extends AbstractComponentHelper implements OnInit, OnDestroy {

  public resetPwdQuesting = false;
  public rResetForm: FormGroup;

  constructor(private _authService: AuthService,
              private _formBuilderService: FormBuilder,
              protected _translateService: TranslateService,
              protected _markupService: MarkupService,
              protected _helperService: HelperService) {

    super(_helperService, _translateService, _markupService);
  }

  ngOnInit() {
    this.initResetForm();
  }

  ngOnDestroy() {
    // remove de modal from the DOM
    $('.ui.dimmer .forgotPwdModal').remove();
  }

  initResetForm() {
    this.messageBloc_ = false;
    this.rResetForm = this._formBuilderService.group({
      'email': [null, [Validators.required, Validators.email]]
    });
  }

  resetPwdSubmit() {
    if (!this.resetPwdQuesting) {
      this.messageBloc_ = false;
      if (this.rResetForm.invalid) {
        this.setErrorMessage_('error.inalidform');
      } else {
        this.resetPwdQuesting = true;
        this._authService.requestResetPwd(this.rResetForm.value).subscribe(
          result => {
            this.resetPwdQuesting = false;
            this.setSuccessMessage_('login.resetPwdSuccessMsg');
            this.rResetForm.reset();
            window.setTimeout(() => {
              $('.forgotPwdModal')
                .modal({
                  onHidden: () => {
                    this.messageBloc_ = false;
                  }
                })
                .modal('hide');
            }, 6000);
          },
          error => {
            this.resetPwdQuesting = false;
            this.handleHttpError_(error);
          },
        );
      }
    }
  }

  isInvalidField(field) {
    return !this.rResetForm.controls[field].valid && this.rResetForm.controls[field].touched;
  }
}
