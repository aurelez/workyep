import { Injectable } from '@angular/core';
import { environment as ENV } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { catchError, retry, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { HelperService } from '../../m_shared/services/helper.service';
import { SharedService } from '../../m_shared/services/shared.service';

@Injectable({ providedIn: 'root' })
export class AuthService {

  constructor(private _httpClient: HttpClient,
              private _helperService: HelperService,
              private _sharedService: SharedService,
              private _routerService: Router) {
  }

  socialAuth(body) {
    return this._httpClient.post<any>(ENV.apiBase + ENV.apiUrl.socialAuth, body)
      .pipe(
        tap(result => { this.setAuthToken(result.oauth); }),
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  wkpAuth(body) {
    return this._httpClient.post<any>(ENV.apiBase + ENV.apiUrl.login, body)
      .pipe(
        tap(result => { this.setAuthToken(result.oauth); }),
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  wkpRegister(body) {
    return this._httpClient.post<any>(ENV.apiBase + ENV.apiUrl.register, body)
      .pipe(
        tap(result => { this.setAuthToken(result.oauth); }),
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  requestResetPwd(body) {
    return this._httpClient.post<any>(ENV.apiBase + ENV.apiUrl.passwordEmailling, body)
      .pipe(
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  refreshToken(body = {}) {
    return this._httpClient.post<any>(ENV.apiBase + ENV.apiUrl.refreshToken, body)
      .pipe(
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  logout() {
    this._httpClient.post<any>(ENV.apiBase + ENV.apiUrl.logout, {})
      .pipe(
        tap(result => {
          this._helperService.setAuth(false);
          this._sharedService.setUserBaseSource(null);
        }),
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      ).subscribe(
        result => { this._routerService.navigate(['']); },
        error => { this._routerService.navigate(['']); }
      );
  }

  // -------
  setAuthToken(token: any) {
    const expDays = 1;
    this._helperService.setCookie(ENV.configs.cookieAccessToken, token.access_token, expDays);
    this._helperService.setAuth(true);
  }
}
