import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

declare global {
  interface Window {
    FB: any;
    fbAsyncInit: any;
  }
}
// window.FB = window.FB || {}; // Non car FB est bind de facon asynchrone

@Injectable({ providedIn: 'root' })
export class FacebookService {

  constructor(private _authService: AuthService) {

    this.init();
  }

  init() {
    (function(d, s, id) {
      // tslint:disable-next-line:no-var-keyword
      // tslint:disable-next-line:prefer-const
      let js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { return; }
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js';
      js.async = true;
      js.defer = true;
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    window.fbAsyncInit = function() {
      window.FB.init({
        appId      : environment.configs.socials.facebook,
        cookie     : true,  // enable cookies to allow the server to access the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.8' // use version 2.8
      });
    };
  }

  login$() {
    return new Observable((observer) => {
      window.FB.login(
        function(authResponse) {
          // console.log(authResponse);
          if (authResponse.status === 'connected') {
            const payload = {
              provider: 'facebook',
              token: authResponse.accessToken
            };

            this._authService.socialAuth(payload).subscribe(
              data => {
                observer.next({ success: true, message: null });
                return {unsubscribe() {}};
              },
              error => {
                observer.next({ success: false, message: error });
                return {unsubscribe() {}};
              }
            );
          } else {
            observer.next({ success: false, message: authResponse });
            return {unsubscribe() {}};
          }

        }.bind(this), { scope : 'email,user_location,public_profile' });
    });
  }

}
