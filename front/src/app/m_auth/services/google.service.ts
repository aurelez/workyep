import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

declare global {
  interface Window { gapi: any; }
}

@Injectable({ providedIn: 'root' })
export class GoogleService {

  public isRequesting = false;

  constructor(private _authService: AuthService) {

    this.init();
  }

  init() {
    const e = document.createElement('script');
    e.type = 'text/javascript';
    e.src = 'https://apis.google.com/js/client:platform.js?onload=gPOnLoad';
    e.async = true;
    e.defer = true;
    document.getElementById('wkp_scripts').appendChild(e);
  }

  login$() {
    return new Observable((observer) => {
      window.gapi.auth.signIn({
        callback: function(authResponse) {
          if (authResponse.status.signed_in) {
            if (authResponse.status.method) { // === 'PROMPT') {
              const payload = {
                provider: 'google',
                token: authResponse.access_token
              };

              if (!this.isRequesting) {
                this.isRequesting = true;
                this._authService.socialAuth(payload).subscribe(
                  data => {
                    observer.next({ success: true, message: null });
                    this.isRequesting = false;
                  },
                  error => {
                    observer.next({ success: false, message: error });
                    this.isRequesting = false;
                  }
                );
              }
            }
          } else {
            this.isRequesting = false;
            observer.next({ success: false, message: authResponse });
            return {unsubscribe() {}};
          }
        }.bind( this ),
        clientid: environment.configs.socials.google,
        cookiepolicy: 'single_host_origin',
        requestvisibleactions: 'http://schema.org/AddAction',
        scope: 'https://www.googleapis.com/auth/plus.login email'
      });
    });
  }

}
