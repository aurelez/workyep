import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

declare global {
  interface Window { IN: any; }
}

@Injectable({ providedIn: 'root' })
export class LinkedinService {

  private maxReinit = 0;

  constructor(private _authService: AuthService) {

    this.init();
  }

  init() {
    (function() {
      const e = document.createElement('script');
      e.type = 'text/javascript';
      e.src = 'http://platform.linkedin.com/in.js?async=true';
      e.async = true;
      e.defer = true;
      document.getElementById('wkp_scripts').appendChild(e);
    })();

    this.reinit();
  }

  reinit() {
    if (!window.IN && this.maxReinit < 20) {
      this.maxReinit++;
      setTimeout(function() {
        this.reinit();
      }.bind(this), 50);
    } else {
      window.IN.init({
        api_key : environment.configs.socials.linkedin,
      });
    }
  }

  login$() {
    const that = this;
    return new Observable((observer) => {
      window.IN.User.authorize( function() {
        window.IN.API.Profile('me')
          .fields(['id', 'email-address', 'firstName', 'lastName', 'pictureUrl', 'publicProfileUrl'])
          .result(result => {
            // console.log(result);
            const authResponse = result.values[0];
            const payload = {
              provider: 'linkedin',
              provider_uid: authResponse.id,
              email: authResponse.emailAddress,
              name: authResponse.firstName,
              last_name: authResponse.lastName,
              avatar: authResponse.pictureUrl
            };

            that._authService.socialAuth(payload).subscribe(
              data => {
                observer.next({ success: true, message: null });
                return {unsubscribe() {}};
              },
              error => {
                observer.next({ success: false, message: error });
                return {unsubscribe() {}};
              }
            );
          })
          .error(function(err) {
            observer.next({ success: false, message: err });
            return {unsubscribe() {}};
          });
      });
    });
  }

}
