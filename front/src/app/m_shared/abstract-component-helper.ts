import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MarkupService, HelperService} from './services';
import { TranslateService } from '@ngx-translate/core';

export abstract class AbstractComponentHelper {

  public messageBloc_: any = false;

  constructor(protected _helperService: HelperService,
              protected _translateService: TranslateService,
              protected _markupService: MarkupService) {

  }

  handleHttpError_(error: HttpErrorResponse) {
    const eEM = this._helperService.extractErrorMessage(error);
    if (eEM.isFatal) {
      this.setErrorMessage_(eEM.data);
    } else { this.messageBloc_ = this._markupService.error(eEM.data.message, eEM.data.mlist); }
  }

  setErrorMessage_(param, mList = null) {
    this._translateService.get(param).subscribe(
      res => { this.messageBloc_ = this._markupService.error(res, mList); }
    );
  }

  setSuccessMessage_(param) {
    this._translateService.get(param).subscribe(
      res => { this.messageBloc_ = this._markupService.sucess(res); }
    );
  }

}
