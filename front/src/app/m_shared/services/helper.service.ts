import { Injectable, isDevMode } from '@angular/core';
import { environment as ENV } from '../../../environments/environment';
import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError as observableThrowError, BehaviorSubject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { SharedService } from './shared.service';

declare const $: any, toastr: any;

const FATALSTATUS = [0, 500];

@Injectable({ providedIn: 'root' })
export class HelperService {

  private isAuthSource = new BehaviorSubject( this.isAuth() );
  public isAuth$ = this.isAuthSource.asObservable();

  constructor(private _translateService: TranslateService,
              private _sharedService: SharedService,
              private _routerService: Router) {

  }

  public handleError(error: HttpErrorResponse) {
    if (isDevMode()) { console.log('handleError', error); }

    switch (error.status) {
      case 401:
        this._sharedService.setInvitLoginMsg(null);
        this._routerService.navigate([ENV.routes.login]);
        break;
      case 0:
      case 500:
        // this._routerService.navigate(['']);
        this._translateService.get('error.fatal').subscribe(r => {
          toastr.error(r);
        });
        break;
    }

    return observableThrowError(error);
  }

  /**
   * @param data
   * @returns i18n params if isFatal is true
   */
  public extractErrorMessage(data: any): any {
    if (FATALSTATUS.includes(data.status)) {
      return { isFatal: true, data: 'error.fatal' };
    } else {
      // tslint:disable-next-line:prefer-const
      let message = 'Error, try later', mList = [];
      if (data.error.message) { message = data.error.message; }
      if (data.error.errors) {
        for (const e in data.error.errors) {
          if (Array.isArray(data.error.errors[e])) {
            data.error.errors[e].forEach(m => { mList.push(m); });
          }
        }
      }
      return { isFatal: false, data: {message: message, mlist: mList} };
    }
  }

  public getHeaders(givenToken = null): HttpHeaders {
    let token;
    if (!givenToken) {
      token = this.getCookie(ENV.configs.cookieAccessToken);
      token = token ? token : 'null';
    } else { token = givenToken; }

    const headers = new HttpHeaders({
      'Accept': 'application/json,application/xml',
      'Accept-Language': this._translateService.currentLang,
      'Authorization': 'Bearer ' + token
    });
    return headers;
  }

  public getCookie(name: string): string {
    const nameLenPlus = (name.length + 1);
    return document.cookie
      .split(';')
      .map(c => c.trim())
      .filter(cookie => {
        return cookie.substring(0, nameLenPlus) === `${name}=`;
      })
      .map(cookie => {
        return decodeURIComponent(cookie.substring(nameLenPlus));
      })[0] || null;
  }

  public setCookie(cname, cvalue, expDays) {
    const d = new Date();
    d.setTime(d.getTime() + (expDays * 24 * 60 * 60 * 1000));
    const expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
  }

  public isAuth(): boolean {
    return this.getCookie(ENV.configs.cookieIsAuth) === 'true';
  }

  public setAuth(auth: boolean) {
    this.setCookie(ENV.configs.cookieIsAuth, auth, 1);
    this.isAuthSource.next(auth);
  }
}
