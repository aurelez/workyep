import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { catchError, switchMap, filter, take, finalize } from 'rxjs/operators';
import { throwError as observableThrowError, Observable, BehaviorSubject } from 'rxjs';

import { environment as ENV } from '../../../environments/environment';
import { HelperService } from '../../m_shared/services/helper.service';
import { AuthService } from '../../m_auth/services/auth.service';

const I18N = 'assets/i18n';
const REFRESH_URL = ENV.apiBase + ENV.apiUrl.refreshToken;
const BYPASS_URL = [
  ENV.apiBase + ENV.apiUrl.login,
  ENV.apiBase + ENV.apiUrl.register
];

@Injectable({ providedIn: 'root' })
export class HttpInterceptorService implements HttpInterceptor {

  public isRefreshingToken = false;
  public tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private _helperService: HelperService,
              private _authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const transformedRequest: HttpRequest<any> = this.addheaders(request);
    return next.handle(transformedRequest)
      .pipe(
        catchError(error => {
          return this.handleError(error, transformedRequest, next);
        })
      );
  }

  addheaders(request: HttpRequest<any>, token = null): HttpRequest<any> {
    if (request.url.includes(I18N)) {return request; }
    const headers = this._helperService.getHeaders(token);
    console.log('HTTP Interceptor:', request.url);
    return request.clone({ headers: headers, withCredentials: true });
  }

  private handleError(error: any, request: HttpRequest<any>, next: HttpHandler) {
    // console.log('InterceptorHandleError:', error, request, next);
    if (error instanceof HttpErrorResponse) {
      switch (error.status) {
        case 401:
          return this.handle401Error(error, request, next);
        case 400:
          return this.handle400Error(error);
        default:
          return observableThrowError(error);
      }
    } else {
      return observableThrowError(error);
    }
  }

  private handle400Error(error: any ) {
    if (error.url === REFRESH_URL) {
      return this.makeItLogout(error);
    }
    return observableThrowError(error);
  }

  private handle401Error(error: any, request: HttpRequest<any>, next: HttpHandler) {
    if (BYPASS_URL.includes(error.url)) { return observableThrowError(error); }
    if (this.isRefreshingToken && error.url === REFRESH_URL) {
      return this.makeItLogout(error);
    }
    return this.refreshAndReSubmit(request, next);
  }

  private refreshAndReSubmit(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;
      // Reset here so that the following requests wait until the token comes back from the refreshToken call.
      this.tokenSubject.next(null);

      return this._authService.refreshToken()
        .pipe(
          switchMap(result => {
            this.tokenSubject.next(result.oauth.access_token);
            // set auth token cookies
            this._authService.setAuthToken(result.oauth);
            // re-execute the original request with the new accessToken
            const newRequest = this.addheaders(request, result.oauth.access_token);
            return next.handle(newRequest);
          }),
          catchError(error => {
            return this.makeItLogout(error);
          }),
          finalize(() => {
            this.isRefreshingToken = false;
          })
        );

    } else {
      return this.tokenSubject
        .pipe(
          filter(token => token != null),
          take(1),
          switchMap(token => {
            const newRequest = this.addheaders(request, token);
            return next.handle(newRequest);
          })
        );
    }
  }

  private makeItLogout(error) {
    this._helperService.setAuth(false);
    return observableThrowError(error);
  }

}
