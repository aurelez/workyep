import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class MarkupService {

  constructor() { }

  error(message, list?: Array<string>) {
    let mList = '';
    if (list) {
      mList = '<ul>';
      list.forEach(m => { mList += '<li>' + m + '</li>'; });
      mList += '</ul>';
    }
    return `
      <div class="ui negative message">
        <div class="header"><i class="ban icon"></i>Oups! <span>${message}</span></div>
        ${mList}
      </div>
    `;
  }

  sucess(message) {
    return `
      <div class="ui success message">
        <div class="header"><i class="hand peace icon"></i>Yep! <span>${message}</span></div>
      </div>
    `;
  }

  phCircleImage(theme) {
    return `
      <div class="ph-item ${theme}">
        <div class="l ph-col-2">
          <div class="ph-avatar"></div>
        </div>
        <div class="r">
          <div class="ph-row">
              <div class="ph-col-12 empty"></div>
            <div class="ph-col-6"></div>
            <div class="ph-col-6 empty"></div>
            <div class="ph-col-12"></div>
          </div>
        </div>
      </div>
    `;
  }

  phSingle(theme = '') {
    return `
      <div class="ph-item single ${theme}">
        <div class="ph-row">
          <div class="ph-col-12"></div>
        </div>
      </div>
    `;
  }
}
