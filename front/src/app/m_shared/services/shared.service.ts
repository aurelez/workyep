import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../../models';

@Injectable({ providedIn: 'root' })
export class SharedService {

  private publicData: any;

  private invitLoginMsgSource = new BehaviorSubject(null);
  public invitLoginMsg$ = this.invitLoginMsgSource.asObservable();

  private isClickToRegisterSource = new BehaviorSubject(false);
  public isClickToRegister$ = this.isClickToRegisterSource.asObservable();

  private userBaseSource = new BehaviorSubject(null);
  public userBase$ = this.userBaseSource.asObservable();
  public userBase: User;

  constructor() { }

  setInvitLoginMsg(v: {msg: string, target: boolean | string}) { this.invitLoginMsgSource.next(v); }
  setIsClickToRegister(v: boolean) { this.isClickToRegisterSource.next(v); }
  setUserBaseSource(v: User) { this.userBase = v; this.userBaseSource.next(v); }
  getUserBase() { return this.userBase; }

  setPublicData(publicData) { this.publicData = publicData; }
  getPublicData() { return this.publicData; }

}
