export class Profile {
    public about: string;
    public available: string;
    public isAvailable: boolean;
    public city: string;
    public country: string;
    public email_check: boolean;
    public experience_year: string;
    public hourly_rate: number;
    public devise: string;
    public response_time: number;
    public created_at: Date;
    public title: string;
    public updated_at: Date;
    public work_out: boolean;
    public skills: any[];

    constructor(profile) {
        this.about = profile.about || '';
        this.available = profile.available || false;
        this.isAvailable = profile.available !== 'not' || false;
        this.city = profile.city || '';
        this.country = profile.country || '';
        this.email_check = profile.email_check === 1;
        this.experience_year = profile.experience_year || '';
        this.hourly_rate = profile.hourly_rate || '';
        this.devise = profile.devise || 'CAD';
        this.response_time = profile.response_time || '';
        this.created_at = profile.created_at || '';
        this.title = profile.title || '';
        this.updated_at = profile.updated_at || '';
        this.work_out = profile.work_out || false;
        this.skills = profile.skills || [];
    }
}
