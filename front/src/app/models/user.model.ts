import { Profile } from './profile.model';

export class User {
    public id: number;
    public active: boolean;
    public avatar: string;
    public created_at: Date;
    public email: string;
    public last_name: string;
    public name: string;
    public role: string;
    public profile: Profile;
    public hasProfile = false;

    constructor(user, profile) {
        this.id = user.id || undefined;
        this.active = user.active || undefined;
        this.avatar = user.avatar || undefined;
        this.created_at = user.created_at || undefined;
        this.email = user.email || undefined;
        this.last_name = user.last_name || undefined;
        this.name = user.name || undefined;
        this.role = user.role || undefined;
        if (profile) {
            this.profile = new Profile(profile);
            this.hasProfile = true;
        } else { this.profile = new Profile({}); }
    }
}
