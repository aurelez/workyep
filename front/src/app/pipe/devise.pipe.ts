import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'devise'
})
export class DevisePipe implements PipeTransform {

  transform(value: number, devise: string): string {
    switch (devise) {
      case 'CAD':
        return `${value}<span class="devise">$CAD</span>`;
      case 'USD':
        return `<span class="devise">$USD</span>${value}`;
      case 'XOF':
        return `${value}<span class="devise">FCFA</span>`;
    }
  }

}
