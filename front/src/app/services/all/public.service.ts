import { Injectable } from '@angular/core';
import { environment as ENV } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';

import { HelperService } from '../../m_shared/services/helper.service';

@Injectable({ providedIn: 'root' })
export class PublicService {

  constructor(private _httpClient: HttpClient,
              private _helperService: HelperService) {

  }

  getAll() {
    return this._httpClient.get<any>(ENV.apiBase + ENV.apiUrl.publicData) // { observe: 'response' }
      .pipe(
        retry(ENV.configs.retryRequest),
        catchError(err => this._helperService.handleError(err)),
      );
  }

  getProfile() {
    return this._httpClient.get<any>(ENV.apiBase + ENV.apiUrl.getProfile)
      .pipe(
        retry(ENV.configs.retryRequest),
        catchError(err => this._helperService.handleError(err)),
      );
  }

  // -- router emailcheck
  emailCheck(body) {
    return this._httpClient.post<any>(ENV.apiBase + ENV.apiUrl.postEmailCheck, body)
      .pipe(
        retry(ENV.configs.retryRequest),
        catchError(err => this._helperService.handleError(err)),
      );
  }

  // -- router resetPassword
  resetPassword(body) {
    return this._httpClient.post<any>(ENV.apiBase + ENV.apiUrl.passwordReset, body)
      .pipe(
        retry(ENV.configs.retryRequest),
        catchError(err => this._helperService.handleError(err)),
      );
  }
}
