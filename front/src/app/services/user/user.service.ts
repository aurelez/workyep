import { Injectable } from '@angular/core';
import { environment as ENV } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, retry, tap, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { User } from '../../models';

import { HelperService } from '../../m_shared/services/helper.service';
import { SharedService } from '../../m_shared/services';

@Injectable({ providedIn: 'root' })
export class UserService {

  constructor(private _httpClient: HttpClient,
              private _helperService: HelperService,
              private _sharedService: SharedService) {
  }

  // -- router getUserBase
  getUserBase(): Observable<User> {
    return this._httpClient.get<any>(ENV.apiBase + ENV.apiUrl.userBase)
      .pipe(
        map(res => this.retUserBase(res) ),
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  // -- router available
  available(body): Observable<User> {
    return this._httpClient.post<any>(ENV.apiBase + ENV.apiUrl.available, body)
      .pipe(
        map(res => {
          const userBase: User = this._sharedService.getUserBase();
          userBase.profile.available = res.available;
          userBase.profile.isAvailable = res.available !== 'not' || false;
          this._sharedService.setUserBaseSource(userBase);
          return userBase;
        }),
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  // -- router changeProfilePhoto
  changeProfilePhoto(body): Observable<User> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'multipart/form-data',
      })
    };
    return this._httpClient.post<any>(ENV.apiBase + ENV.apiUrl.profilePhoto, body, httpOptions)
      .pipe(
        map(res => {
          const userBase: User = this._sharedService.getUserBase();
          userBase.avatar  = res.avatar;
          this._sharedService.setUserBaseSource(userBase);
          return userBase;
        }),
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  // -- router emailCheck
  getEmailCheck(): Observable<any> {
    return this._httpClient.get<any>(ENV.apiBase + ENV.apiUrl.getEmailCheck)
      .pipe(
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  // -- router addSkill
  addSkill(body): Observable<any> {
    return this._httpClient.post(ENV.apiBase + ENV.apiUrl.addSkill, body)
      .pipe(
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  // -- router removeSkill
  removeSkill(id): Observable<any> {
    return this._httpClient.delete(ENV.apiBase + ENV.apiUrl.removeSkill + id)
      .pipe(
        catchError(err => this._helperService.handleError(err)),
        retry(ENV.configs.retryRequest)
      );
  }

  // -- parts
  retUserBase(res: any) {
    const user = new User(res.data.user, res.data.profile);
    this._sharedService.setUserBaseSource(user);
    return user;
  }

}
