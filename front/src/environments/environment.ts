// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiBase:  'http://localhost:8000/api/',
  configs: Object.freeze({
    retryRequest: 0,
    cookieAccessToken: 'WkpAccessToken',
    cookieIsAuth: 'WkpIsAuth',
    socials: {
      google: '784455569932-8td6n1e4lka85j352k4rthhacje9cjqf',
      facebook: '582686828762762',
      linkedin: '776s1mk0bjoeu0',
      twitter: 'HyEnlFbl8pZ2ihx2kmTltSBDt'
    },
    primaryColorRgb: '24, 143, 167',
    primaryColorEx: '#188FA7',
    maxFileSize: 6242880
  }),

  apiUrl: {
    // Auth
    login:             'auth/login',
    logout:            'auth/logout',
    register:          'auth/register',
    socialAuth:        'auth/social',
    refreshToken:      'auth/refresh',
    passwordEmailling: 'auth/password/email',
    passwordReset:     'auth/password/reset',
    // Public
    publicData:        'forall/publicdata',
    getProfile:        'forall/profile/',
    postEmailCheck:    'forall/emailcheck',
    // User
    userBase:           'user/base',
    available:          'user/available',
    profilePhoto:       'user/profilephoto',
    getEmailCheck:      'user/emailcheck',
    findSkills:         'user/findskills',
    addSkill:           'user/skill',
    removeSkill:        'user/skill?id=',
  },

  routes: {
    login: 'login',
    restePwd: 'password-reset',
    userDashboard: 'user/dashboard',
    action: 'action/:action'
  }
};
